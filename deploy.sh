#!/bin/sh

if [[ -z $1 ]]
  then
    echo "Running dry-run"
    rsync --dry-run -az --force --delete --progress --exclude-from=rsync_exclude.txt ./ gilleschandelier@dold.cyrilchandelier.com:/home/gilleschandelier/www/game-server
elif [ $1 == "go" ]
  then
    echo "Running actual deploy"
    rsync -az --force --delete --progress --exclude-from=rsync_exclude.txt ./ gilleschandelier@dold.cyrilchandelier.com:/home/gilleschandelier/www/game-server
    ssh gilleschandelier@dold.cyrilchandelier.com 'cd /home/gilleschandelier/www/server; npm install; ./launch.sh'
else
    echo "Missing parameters"
fi