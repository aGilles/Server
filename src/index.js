import http from "http";
import express from "express";
import cors from "cors";
import morgan from "morgan";
import bodyParser from "body-parser";
import middleware from "./middleware";
import api from "./api";
import config from "./config.json";
import authenticationMiddleware from "./middleware/auth";
const jwt = require("jsonwebtoken");
const database = require("./utils/db");
const router = require("./utils/router");

var mongoose = require("mongoose");
mongoose.connect("mongodb://localhost/test", { useNewUrlParser: true });

const app = require("express")();
const server = require("http").Server(app);
export const io = require("socket.io")(server, {
  pingInterval: 3000,
  pingTimeout: 2000,
});

// logger
app.use(morgan("dev"));

// 3rd party middleware
app.use(
  cors({
    exposedHeaders: config.corsHeaders,
  })
);

app.use(
  bodyParser.json({
    limit: config.bodyLimit,
  })
);

app.use(authenticationMiddleware);

initDatabaseMiddleWare();
console.log(process.cwd);
router.load(app, "./src/controllers");

function initDatabaseMiddleWare() {
  if (process.platform === "win32") {
    require("readline")
      .createInterface({
        input: process.stdin,
        output: process.stdout,
      })
      .on("SIGINT", () => {
        console.log("SIGINT: Closing MongoDB connection");
        database.close();
      });
  }

  process.on("SIGINT", () => {
    console.log("SIGINT: Closing MongoDB connection");
    database.close();
  });

  database.open(() => {});
}

/*connect to db
initializeDb(db => {
  //handleConnection(app.server, db);
  // internal middleware
  //app.use(middleware({ config, db }));
  // api router
  //app.use("/api", api({ config, db }));
});*/
server.listen(process.env.PORT || config.port, () => {
  console.log(`Started on port ${server.address().port}`);
});

const User = require("./models/user");

io.use(async (socket, next) => {
  const token = socket.handshake.query.token;
  console.log("*****");
  const decoded = jwt.verify(token, "secretkeyforthegame");
  const { _id: userId } = decoded;

  // Try to retrieve user
  const user = await User.findOne({ _id: userId, token: token }).catch(
    (error) => {
      console.log("TOKEN EXPIRED");
      socket.emit("tokenExpired");
      socket.disconnect();
      throw error;
    }
  );

  for (const socketID in io.sockets.connected) {
    let socket = io.sockets.sockets[socketID];
    if (socket._id === userId) {
      console.log("CRUSHING CONNECTION");
      socket.emit("crushingConnection");
      socket.disconnect();
    }
  }

  // Update socket
  socket.user = user;
  socket._id = decoded._id;

  next();
});

require("./utils/socket_manager")(io);
export default app;
