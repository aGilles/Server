const usersProvider = require("../../utils/providers/user.provider");
const regeneratorRuntime = require("regenerator-runtime");
const jwt = require("jsonwebtoken");

import { io } from "../../index";

/**
 * Controller used by the server allowing access to the users management.
 *
 * Accessible via the routes '/users'
 **/
class UsersController {
  /**
   * Users controller add routes to manage users data
   *
   * Routes accessible from /question :
   * - {get} / : Get all the stocked users
   * - {get} /:id : Get a specific uer by its ID
   * - {post} / : add a new user
   * - {post} /advanced : update advanced details of an user
   * - {post} /rates : update rates of an user
   * - {post} /money : update money of an user
   * - {post} /history : add a bet to the history of bets to an user
   * @param {Router} router Express router
   */
  constructor(router) {
    router.get("/", this.getUsers.bind(this));
    router.put("/defaultRate", this.setRatingValueAll.bind(this));
    router.get("/token", this.authToken.bind(this));
    router.post("/add", this.addUser.bind(this));
    router.post("/login", this.login.bind(this));
    router.get("/ranking/dupz", this.getRankingDupz.bind(this));
    router.get("/ranking/fun", this.getRankingFun.bind(this));

    router.get("/:id", this.getUserById.bind(this));
    //router.post('/:id', this.answerQuestion.bind(this));
    router.post("/advanced", this.updateUserAdvanced.bind(this));
    router.post("/rates", this.updateUserRates.bind(this));
    router.post("/money", this.updateUserMoney.bind(this));
    router.post("/history", this.addBetHistory.bind(this));
    router.post("/status", this.updateUserStatus.bind(this));
  }

  getUsers(req, res) {
    usersProvider.getUsers((err, users) => {
      if (err) {
        console.log("*** user.provider.getUser error: " + err);
        res.json({ error: "Insert failed", users: null });
      } else {
        console.log("*** getUser ok");
        res.json(users);
      }
    });
  }

  getUserById(req, res) {
    usersProvider.getUserById(req.params.id, (err, user) => {
      if (err) {
        console.log("*** user.provider.getUser error: " + err);
        res.json({ error: "Insert failed", user: null });
      } else {
        console.log("*** getUser ok");
        res.json(user);
      }
    });
  }

  login(req, res) {
    usersProvider.getUserByPseudo(req.body.name, async (err, user) => {
      if (err) {
        console.log("*** user.controller.login error: " + err);
        res.json({ error: err, user: null });
      } else {
        if (user != null) {
          user.checkValidCredentials(
            req.body.password,
            async (err, success) => {
              if (err) {
                console.log(
                  "*** user.controller.login.checkvalidcredentials error: " +
                    err
                );
                res.json({ error: err });
              }
              if (!success) {
                res.json({ error: "Mot de passe incorrect" });
              } else {
                try {
                  for (const socketID in io.sockets.connected) {
                    let socket = io.sockets.sockets[socketID];
                    if (socket.user._id === user._id) {
                      socket.emit("crushingConnection");
                      socket.disconnect();
                    }
                  }
                } catch (error) {
                  console.log(error);
                }

                const token = await user.newAuthToken();
                res.json({ user, token });
              }
            }
          );
        } else {
          res.json({ error: "Pseudo non existant" });
        }
      }
    });
  }

  getRankingDupz(req, res) {
    usersProvider.getDupzRanking((err, users) => {
      if (err) {
        console.log("*** user.provider.getUser error: " + err);
        res.json({ error: err, users: null });
      } else {
        res.json(users);
      }
    });
  }

  getRankingFun(req, res) {
    usersProvider.getFunRanking((err, users) => {
      if (err) {
        console.log("*** user.provider.getUser error: " + err);
        res.json({ error: err, users: null });
      } else {
        res.json(users);
      }
    });
  }

  authToken(req, res) {
    res.status(200);
  }

  addUser(req, res) {
    usersProvider.addUser(req.body, async (err, user) => {
      if (err) {
        console.log("*** user.provider.addUser error: " + err);
        res.json({ error: err, user: null });
      } else {
        console.log("*** addUser ok");
        try {
          const token = await user.newAuthToken();
          res.status(201).send({ user, token });
        } catch (e) {
          res.status(400).send(e);
        }
      }
    });
  }

  setRatingValueAll(req, res) {
    usersProvider.updateAllUsersRating((err) => {
      if (err) {
        console.log("*** user.provider.addUser error: " + err);
        res.json({ error: err, user: null });
      } else {
        res.send("C bon");
      }
    });
  }

  addBetHistory(req, res) {
    usersProvider.addBetHistory(req.body, (err, user) => {
      if (err) {
        console.log("*** user.provider.addBetHistory error: " + err);
        res.json({ error: "Insert failed", user: null });
      } else {
        console.log("*** addBetHistory ok");
        res.json(user);
      }
    });
  }

  updateUserMoney(req, res) {
    usersProvider.updateUserMoney(req.body, (err, user) => {
      if (err) {
        console.log("*** user.provider.updateUserMoney error: " + err);
        res.json({ error: "Insert failed", user: null });
      } else {
        console.log("*** updateUserMoney ok");
        res.json(user);
      }
    });
  }

  updateUserStatus(req, res) {
    usersProvider.updateUserStatus(req.body, (err, user) => {
      if (err) {
        console.log("*** user.provider.updateUserStatus error: " + err);
        res.json({ error: "Insert failed", user: null });
      } else {
        console.log(user);
        console.log("*** updateUserStatus ok");
        res.json(user);
      }
    });
  }

  updateUserRates(req, res) {
    usersProvider.updateUserRates(req.body, (err, user) => {
      if (err) {
        console.log("*** user.provider.updateUsersRates error: " + err);
        res.json({ error: "Insert failed", user: null });
      } else {
        console.log("*** updateUsersRates ok");
        res.json(user);
      }
    });
  }

  updateUserAdvanced(req, res) {
    usersProvider.updateUserAdvanced(req.body, (err, user) => {
      if (err) {
        console.log("*** user.provider.updateUserrAdvanced error: " + err);
        res.json({ error: "Insert failed", user: null });
      } else {
        console.log("*** updateUserrAdvanced ok");
        res.json(user);
      }
    });
  }
}
module.exports = UsersController;
