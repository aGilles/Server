export default class Player {
  name;
  number;
  points;
  energy;
  id;
  gotCanceled = false;
  reloading = false;

  constructor(name, number, id) {
    this.name = name;
    this.number = number;
    this.points = 0;
    this.energy = 3;
    this.id = id;
  }

  incrementEnergy() {
    this.energy++;
  }

  decreaseEnergy(n) {
    this.energy -= n;
  }

  undoCancel() {
    this.gotCanceled = false;
  }

  setCanceled() {
    this.gotCanceled = true;
  }

  getName() {
    return this.name;
  }

  getNumber() {
    return this.number;
  }

  getPlayer() {
    return { name: this.name, number: this.number, points: this.points };
  }
}
