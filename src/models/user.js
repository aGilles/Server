const mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  ObjectId = Schema.Types.ObjectId;

const bcrypt = require("bcryptjs");
const jwt = require("jsonwebtoken");

const UserSchema = new Schema({
  _id: { type: ObjectId, require: true },
  name: { type: String, required: true },
  password: { type: String, required: true },
  rating: { type: Number, required: false },
  ratingFun: { type: Number, required: false },
  dupzHistory: {
    games: { type: Number, default: 0 },
    losses: { type: Number, default: 0 },
    draws: { type: Number, default: 0 },
    victories: { type: Number, default: 0 },
  },
  funHistory: {
    games: { type: Number, default: 0 },
    losses: { type: Number, default: 0 },
    draws: { type: Number, default: 0 },
    victories: { type: Number, default: 0 },
  },

  token: {
    type: String,
  },
  createdAt: {
    type: Date,
    default: Date.now,
  },
});

UserSchema.methods.newAuthToken = async function () {
  const user = this;
  const token = jwt.sign({ _id: user.id.toString() }, "secretkeyforthegame", {
    expiresIn: "7 days",
  });
  user.token = token;
  await user.save();
  return token;
};

UserSchema.methods.checkValidCredentials = async function (
  candidatePassword,
  callback
) {
  bcrypt.compare(candidatePassword, this.password, (err, isMatch) => {
    if (err) {
      console.log("User.checkvalidcredentials error" + err);
      callback(err);
    }
    return callback(null, isMatch);
  });
};

UserSchema.pre("save", async function (next) {
  const user = this;
  if (user.isModified("password")) {
    user.password = await bcrypt.hash(user.password, 8);
  }
  next();
});

mongoose.models = {};

module.exports = mongoose.model("User", UserSchema);
