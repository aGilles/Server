import Grid, { Type } from "./Grid";
const userProvider = require("../utils/providers/user.provider");
const Elo = require("arpad");
import { v4 as uuidv4 } from "uuid";

import { dupzMaps } from "../utils/maps/dupzMaps";
import { funMaps } from "../utils/maps/funMaps";
import { classiqueMaps } from "../utils/maps/classiqueMaps";

//TODO WHEN REFACTOR: HAVE A STATE ENUM WESH
export default class Game {
  grid;
  players = [];
  endGame = false;
  created = false;
  started = false;
  playersReady = 0;
  id;
  lastPlayerPlayed;
  moveOnGoing = false;
  type;
  firstPlayer;
  winner;
  paused = false;

  constructor(players, type) {
    this.grid = new Grid();
    this.players = players;
    this.id = uuidv4();
    this.type = type;
  }

  getGame() {
    return {
      grid: this.grid,
      players: this.players,
      id: this.id,
    };
  }
  setGrid(grid) {
    this.grid = grid;
  }

  setDisconnected(type) {
    this.players[type].id = "disconnected";
  }

  setReconnected(type, id) {
    this.grid.grid.forEach((cell) => {
      if (cell.type === type) {
        cell.idPlayer = id;
      }
    });
    this.players[type].id = id;
  }

  calculateNewRatings() {
    if (this.type === "bacteria") {
      return;
    }
    var uscf = {
      default: 32,
      2100: 24,
      2400: 16,
    };

    var min_score = 100;
    var max_score = 10000;

    var elo = new Elo(uscf, min_score, max_score);

    const player1Rating = this.players[0].rating;
    const player2Rating = this.players[1].rating;

    console.log(player1Rating);

    const oddsPlayer1Won = elo.expectedScore(player1Rating, player2Rating);
    const oddsPlayer2Won = elo.expectedScore(player2Rating, player1Rating);
    console.log(oddsPlayer1Won);
    if (!this.winner) {
      this.players[0].rating = elo.newRating(
        oddsPlayer1Won,
        0.5,
        player1Rating
      );

      this.players[1].rating = elo.newRating(
        oddsPlayer2Won,
        0.5,
        player2Rating
      );
      this.updateUserRating(0, false, false);
      this.updateUserRating(1, false, false);
    } else if (this.winner === this.players[0]) {
      this.players[0].rating = elo.newRating(oddsPlayer1Won, 1, player1Rating);
      this.players[1].rating = elo.newRating(oddsPlayer2Won, 0, player2Rating);
      console.log(elo.newRating(oddsPlayer1Won, 1, player1Rating));

      this.updateUserRating(0, true, false);
      this.updateUserRating(1, false, true);
    } else {
      this.players[0].rating = elo.newRating(oddsPlayer1Won, 0, player1Rating);
      this.players[1].rating = elo.newRating(oddsPlayer2Won, 1, player2Rating);
      this.updateUserRating(0, false, true);
      this.updateUserRating(1, true, false);
    }
  }

  updateUserRating(player, hasWon, hasLost) {
    if (this.type === "bacteria") {
      return;
    }
    console.log(this.players[player]);
    const { name, rating } = this.players[player];
    userProvider.updateUserRatingAndHistory(
      name,
      rating,
      this.type,
      hasWon,
      hasLost,
      (err) => {
        if (err) {
          console.log("*** update user rating ***" + err);
          throw new Error();
        }
        console.log("DONE UPDATE USER RATING");
      }
    );
  }

  setEndGame(type, disconnect) {
    this.grid.grid.forEach((cell) => {
      if (cell.type === Type.EMPTY) {
        cell.type = type;
        this.players[type].points++;
      }
    });
    this.endGame = true;
    //if disconnect = null we don't set the winner now
    if (disconnect && type === 0) {
      return (this.winner = this.players[0]);
    } else if (disconnect && type === 1) {
      return (this.winner = this.players[1]);
    }
    //if draw we don't set a winner
    if (this.players[0].points > this.players[1].points) {
      return (this.winner = this.players[0]);
    } else if (this.players[0].points < this.players[1].points) {
      return (this.winner = this.players[1]);
    }
  }

  setLastPlayerPlayed(idPlayer) {
    this.lastPlayerPlayed = idPlayer;
  }

  updatePlayers(players) {
    this.players = players;
  }

  endDetection() {
    if (this.players[0].points === 0) {
      this.setEndGame(1);
      this.calculateNewRatings();
      return true;
    }
    if (this.players[1].points === 0) {
      this.setEndGame(0);
      this.calculateNewRatings();

      return true;
    }
    if (!this.isThereAtLeastOneMove(0)) {
      this.setEndGame(1);
      this.calculateNewRatings();

      return true;
    }
    if (!this.isThereAtLeastOneMove(1)) {
      this.setEndGame(0);
      this.calculateNewRatings();

      return true;
    }

    return false;
  }

  isThereAtLeastOneMove(idPlayer) {
    let gridToSimulate = this.grid.grid;
    let end = false;

    gridToSimulate
      .filter((cell) => cell.type === this.players[idPlayer].number)
      .forEach((cell) => {
        gridToSimulate
          .filter(
            (cell2) =>
              Grid.adjacentTest(cell, cell2) || Grid.jumpTest(cell, cell2)
          )
          .forEach((cell3) => {
            if (cell3.type === Type.EMPTY || cell3.type === Type.Hole)
              end = true;
          });
      });

    return end;
  }

  duplicatePiece(cellFrom, cellTo) {
    //set the type that's gonna eat
    const typeTo = cellFrom.type;
    const idTo = this.players.find((player) => player.id === cellFrom.idPlayer)
      .id;

    //change type of empty cell
    this.grid.changeType(cellTo, typeTo, idTo);
    this.players[typeTo].points++;

    this.eatPieces(cellTo, typeTo, idTo);
    this.players[cellFrom.type].decreaseEnergy(1);
  }

  jumpPiece(cellFrom, cellTo) {
    //set the type that's gonna eat
    const typeTo = cellFrom.type;
    const idTo = this.players.find((player) => player.id === cellFrom.idPlayer)
      .id;

    //change type of empty cell
    this.grid.changeType(cellTo, typeTo, idTo);

    //change type of the cell that loses the piece with the jump
    if (cellFrom.shield === 1) {
      this.grid.grid[cellFrom.id].shield = 0;
    } else {
      this.grid.changeType(cellFrom, Type.EMPTY, 0);
    }

    this.eatPieces(cellTo, typeTo, idTo);

    this.players[cellFrom.type].decreaseEnergy(1);
  }

  eatPieces(cellTo, typeTo, idTo) {
    // set the type that's gonna be eaten
    let typeToEat;
    if (typeTo === Type.PLAYER1) typeToEat = Type.PLAYER2;
    else typeToEat = Type.PLAYER1;

    //eat only the adjacents to the destination cell which contains the typeToEat
    this.grid.grid = this.grid.grid.map((cell) => {
      if (Grid.adjacentTest(cell, cellTo) && cell.type === typeToEat) {
        if (cell.shield === 1) {
          cell.shield = 0;
          return cell;
        }
        this.grid.changeType(cell, typeTo, idTo);
        this.players[typeToEat].points--;
        this.players[typeTo].points++;
      }
      return cell;
    });
  }

  giveShield(cell) {
    this.grid.grid[cell.id].shield = 1;
    this.players[cell.type].decreaseEnergy(1);
  }

  startGame() {
    this.started = true;
  }

  async retrieveRatings() {
    if (this.type === "bacteria") {
      return;
    }
    await new Promise((resolve, reject) => {
      userProvider.getUserByPseudo(this.players[0].name, (err, user) => {
        if (err) {
          console.log("*** get user player 1 error: " + err);
          throw new Error();
        } else {
          if (this.type === "dupz") {
            this.players[0].rating = user.rating;
          } else if (this.type === "fun") {
            this.players[0].rating = user.ratingFun;
          }
          resolve();
        }
      });
    });

    return new Promise((resolve, reject) => {
      userProvider.getUserByPseudo(this.players[1].name, (err, user) => {
        if (err) {
          console.log("*** get User player 2 error:" + err);
          throw new Error();
        } else {
          if (this.type === "dupz") {
            this.players[1].rating = user.rating;
          } else if (this.type === "fun") {
            this.players[1].rating = user.ratingFun;
          }
          resolve();
        }
      });
    });
  }

  createMap() {
    let maps = [];
    switch (this.type) {
      case "dupz":
        maps = dupzMaps;
        break;
      case "fun":
        maps = funMaps;
        break;
      case "bacteria":
        maps = classiqueMaps;
        break;
    }
    const indexMap = Math.floor(Math.random() * maps.length);
    this.makeMap(maps[indexMap]);

    this.created = true;
  }

  makeMap(map) {
    this.grid.createGrid(map.length, map.length);
    let index = 0;
    map.forEach((line) => {
      const cells = line.split(",");
      cells.forEach((cell) => {
        switch (cell) {
          case "1":
            this.grid.addPiece(index++, Type.PLAYER1, this.players[0].id);
            this.players[0].points++;
            break;
          case "2":
            this.grid.addPiece(index++, Type.PLAYER2, this.players[1].id);
            this.players[1].points++;
            break;
          case "X":
          case "x":
            this.grid.addHole(index++);
            break;
          case "0":
            index++;
            break;
          default:
            break;
        }
      });
    });
  }

  createMap1(id1, id2) {
    this.grid.createGrid(9, 9);
    this.grid.addPiece(0, Type.PLAYER1, id1);
    this.grid.addPiece(1, Type.PLAYER1, id1);

    this.players[0].points += 2;
    this.grid.addPiece(79, Type.PLAYER2, id2);
    this.grid.addPiece(80, Type.PLAYER2, id2);
    this.players[1].points += 2;
    this.grid.addHole(30);
    this.grid.addHole(31);
    this.grid.addHole(32);
    this.grid.addHole(39);
    this.grid.addHole(40);
    this.grid.addHole(41);
    this.grid.addHole(48);
    this.grid.addHole(49);
    this.grid.addHole(50);
  }

  createMap2(id1, id2) {
    this.grid.createGrid(7, 7);
    this.grid.addPiece(0, Type.PLAYER1, id1);
    this.players[0].points++;
    this.grid.addPiece(48, Type.PLAYER2, id2);
    this.players[1].points++;

    this.grid.addHole(24);
  }

  createMap4(id1, id2) {
    this.grid.createGrid(10, 10);
    this.grid.addPiece(0, Type.PLAYER1, id1);
    this.grid.addPiece(9, Type.PLAYER1, id1);

    this.players[0].points += 2;
    this.grid.addPiece(90, Type.PLAYER2, id2);
    this.grid.addPiece(99, Type.PLAYER2, id2);
    this.players[1].points += 2;

    this.grid.addHole(44);
    this.grid.addHole(45);
    this.grid.addHole(54);
    this.grid.addHole(55);
  }

  createMap3(id1, id2) {
    this.grid.createGrid(6, 6);
    this.grid.addPiece(0, Type.PLAYER1, id1);
    this.players[0].points++;
    this.grid.addPiece(35, Type.PLAYER2, id2);
    this.players[1].points++;
    this.grid.addHole(7);
    this.grid.addHole(8);
    this.grid.addHole(9);
    this.grid.addHole(10);
    this.grid.addHole(13);
    this.grid.addHole(16);
    this.grid.addHole(19);
    this.grid.addHole(22);
    this.grid.addHole(28);
    this.grid.addHole(27);
    this.grid.addHole(26);
    this.grid.addHole(25);
  }
}

export const Turn = {
  PLAYER1: "player1",
  PLAYER2: "player2",
};
