export default class Grid {
  grid = [];

  constructor() {
    this.addPiece = this.addPiece.bind(this);
  }

  createGrid(rows, cols) {
    this.grid = [];
    let type = Type.EMPTY;
    let id = 0;
    for (let i = 0; i < rows; i++) {
      for (let j = 0; j < cols; j++) {
        const coords = { i, j };
        let cell = { coords, type, id, idPlayer: 0, shield: 0 };
        this.grid.push(cell);
        id++;
      }
    }
  }

  changeType(cell, typeTo, idTo) {
    this.grid[cell.id].type = typeTo;
    this.grid[cell.id].idPlayer = idTo;
  }

  addHole(id) {
    this.grid[id].type = Type.HOLE;
  }

  addPiece(id, type, idPlayer) {
    this.grid[id].type = type;
    this.grid[id].idPlayer = idPlayer;
  }

  getGrid() {
    return this.grid;
  }

  static adjacentTest(cellFrom, cellTo) {
    if (
      Math.abs(cellFrom.coords.i - cellTo.coords.i) <= 1 &&
      Math.abs(cellFrom.coords.j - cellTo.coords.j) <= 1
    )
      return true;
    return false;
  }

  static jumpTest(cellFrom, cellTo) {
    const jumpRow =
      Math.abs(cellFrom.coords.i - cellTo.coords.i) === 2 &&
      cellFrom.coords.j === cellTo.coords.j;
    const jumpColumn =
      Math.abs(cellFrom.coords.j - cellTo.coords.j) === 2 &&
      cellFrom.coords.i === cellTo.coords.i;
    if (jumpRow || jumpColumn) return true;
    return false;
  }
}

export const Type = {
  EMPTY: "empty",
  PLAYER1: 0,
  PLAYER2: 1,
  HOLE: "hole"
};
