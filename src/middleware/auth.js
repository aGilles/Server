const jwt = require("jsonwebtoken");

const User = require("../models/user");

const AuthenticationMiddleware = async (request, response, next) => {
  if (
    request.originalUrl === "/src/controllers/users/login" ||
    "/src/controllers/users/add"
  ) {
    return next();
  }
  try {
    console.log("yoooo");
    // Extract user id from token
    const token = request.header("Authorization").replace("Bearer ", "");
    console.log(token);
    const decoded = jwt.verify(token, "secretkeyforthegame");
    const { _id: userId } = decoded;

    // Try to retrieve user
    const user = await User.findOne({ _id: userId, token: token }).catch(
      (error) => {
        throw error;
      }
    );

    // Update request
    request.token = token;
    request.user = user;

    next();
  } catch (error) {
    console.error(error);
    response.status(401).send({ error: "Please authenticate!" });
  }
};

module.exports = AuthenticationMiddleware;
