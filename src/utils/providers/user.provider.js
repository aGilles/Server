const mongoose = require("mongoose"),
  Schema = mongoose.Schema,
  User = require("../../models/user");

const bcrypt = require("bcryptjs");
/**
 * Utility class userween the controller and model. Manage the Question data.
 */
class UserProvider {
  getUsers(callback) {
    console.log("*** UserProvider.getUsers");
    User.find({})
      .sort({ rating: "descending" })
      .exec((err, users) => {
        if (err) {
          console.log(`*** usersProvider.getUsers error: ${err}`);
          return callback(err);
        }
        callback(null, users);
      });
  }

  addUser(data, callback) {
    console.log("*** UserProvider.addUser");

    this.getUserByPseudo(data.name, (err, user) => {
      if (err) {
        callback(err);
      } else {
        if (user != null) {
          console.log(
            "*** UsersProvider addUser error: Already existing username error"
          );
          callback("Pseudo deja existant");
        } else {
          let user = new User();
          user._id = new mongoose.Types.ObjectId();
          user.name = data.name;
          user.password = data.password;
          user.rating = 1200;
          user.ratingFun = 1200;

          user.save((err) => {
            if (err) {
              console.log(`*** UsersProvider addUser error: ${err}`);
              return callback(err, null);
            } else {
              return callback(null, user);
            }
          });
        }
      }
    });
  }
  getUserById(id, callback) {
    console.log("*** UserProvider.getUserById");
    User.findById(id).exec((err, user) => {
      if (err) {
        console.log(`*** usersProvider.launchuser error: ${err}`);
        return callback(err);
      }
      callback(null, user);
    });
  }

  getUserByPseudo(_pseudo, callback) {
    console.log("*** UserProvider.getUserById");
    User.findOne({ name: { $regex: new RegExp(_pseudo, "i") } }).exec(
      (err, user) => {
        if (err) {
          console.log(`*** usersProvider.launchuser error: ${err}`);
          return callback(err);
        }
        callback(null, user);
      }
    );
  }

  getDupzRanking(callback) {
    console.log("*** UserProvider.getDupzRanking");
    User.find({ "dupzHistory.games": { $gt: 0 } })
      .sort({ rating: -1 })
      .exec((err, users) => {
        if (err) {
          console.log(`*** usersProvider.getUsers error: ${err}`);
          return callback(err);
        }
        callback(null, users);
      });
  }

  getFunRanking(callback) {
    console.log("*** UserProvider.getUsers");
    User.find({ "funHistory.games": { $gt: 0 } })
      .sort({ ratingFun: -1 })
      .exec((err, users) => {
        if (err) {
          console.log(`*** usersProvider.getUsers error: ${err}`);
          return callback(err);
        }
        callback(null, users);
      });
  }

  updateAllUsersRating(callback) {
    User.find({}).exec((err, users) => {
      if (err) {
        console.log(`*** UsersProvider.updateUserRates error: ${err}`);
        return callback(err);
      }

      users.forEach((user) => {
        user.ratingFun = 1200;
        user.save((err) => {
          if (err) {
            return callback(err);
          }
        });
      });
      callback(null);
    });
  }

  updateUserRatingAndHistory(name, rating, type, hasWon, hasLost, callback) {
    console.log("*** UserProvider.updateUserRates");
    User.findOne({ name: { $regex: new RegExp(name, "i") } }).exec(
      (err, user) => {
        if (err) {
          console.log(`*** UsersProvider.updateUserRates error: ${err}`);
          return callback(err);
        }
        if (type === "dupz") {
          user.rating = rating;
          user.dupzHistory.games++;

          if (hasWon) {
            user.dupzHistory.victories++;
          } else if (hasLost) {
            user.dupzHistory.losses++;
          } else {
            user.dupzHistory.draws++;
          }
        } else if (type === "fun") {
          user.ratingFun = rating;
          user.funHistory.games++;

          if (hasWon) {
            user.funHistory.victories++;
          } else if (hasLost) {
            user.funHistory.losses++;
          } else {
            user.funHistory.draws++;
          }
        }

        user.save((err) => {
          if (err) {
            return callback(err);
          }
          callback(null);
        });
      }
    );
  }
}

module.exports = new UserProvider();
