import Player from "../models/Player";
import Game from "../models/Game";
import { Type } from "../models/Grid";
import * as d3Timer from "d3-timer";

let gamesWaiting = { dupz: [], fun: [], bacteria: [] };
let gamesOnGoing = {};
let numberUsers = 0;

module.exports = (io) => {
  io.on("connection", (socket) => {
    socket.on("disconnect", handleDeconnection);

    socket.on("joinRoom", handleReconnection);

    socket.on("leaveRoom", handleLeaveRoom);

    socket.on("messageGame", (data) => {
      console.log("***** MESSAGE CHAT ***** " + socket.id);
      console.log(data);
      io.to(data.gameId).emit("messageGame", {
        id: socket.id,
        content: data.content,
      });
    });
    console.log("connection from socket", socket.id);
    if (socket.user) {
      console.log("connection from user", socket.user.name);
    }

    numberUsers++;
    socket.on("create_game", handleCreate);

    socket.on("duplication", handleDuplication);

    /*socket.on("tryConnect", data => {
      checkDb(socket, db, data.data.username, data.data.password, callback);
    });*/

    socket.on("ready", handleStart);
    socket.on("jump", handleJump);
    socket.on("move", handleMove);

    socket.on("shield", handleShield);

    updateNumbers();

    async function joinGame(data) {
      const { type } = data;
      if (gamesWaiting[type].length > 0) {
        console.log(`****** JOINING GAME : ${type} ******** ${socket.id}`);
        let player2 = new Player(data.pseudo, 1, socket.id);
        socket.join(gamesWaiting[type][0].id);
        if (gamesWaiting[type][0].players[0].id === socket.id) {
          gamesWaiting[type].shift();
          return false;
        }
        gamesWaiting[type][0].players.push(player2);

        if (type === "bacteria") {
          gamesWaiting[type][0].started = true;
          if (Math.floor(Math.random() * Math.floor(2)) === 0) {
            gamesWaiting[type][0].firstPlayer =
              gamesWaiting[type][0].players[0].id;
          } else {
            gamesWaiting[type][0].firstPlayer =
              gamesWaiting[type][0].players[1].id;
          }
        }
        await gamesWaiting[type][0].createMap();
        await gamesWaiting[type][0].retrieveRatings();

        io.to(gamesWaiting[type][0].id).emit("game_created", {
          game: gamesWaiting[type][0],
        });
        gamesOnGoing = {
          ...gamesOnGoing,
          [gamesWaiting[type][0].id]: { game: gamesWaiting[type][0] },
        };
        gamesWaiting[type].shift();
        updateNumbers();
        return true;
      }
      return false;
    }

    async function handleCreate(data) {
      //if there is a game waiting for one player, add the requesting player to it

      // if not, create a new game and puts it on waitingGames list
      if (await joinGame(data)) return;
      console.log(
        `****** CREATE WAITING GAME : ${data.type} ******** ${socket.id}`
      );
      const player1 = new Player(data.pseudo, 0, socket.id);
      let newGame = new Game([player1], data.type);
      socket.join(newGame.id);

      gamesWaiting[data.type].push(newGame);
      updateNumbers();
    }

    function handleDuplication(data) {
      handleMove(socket, data, "dup");
    }

    function handleJump(data) {
      handleMove(socket, data, "jump");
    }

    function handleStart(data) {
      console.log("***** READY *****");
      console.log(data);
      const { gameId } = data;

      if (!gamesOnGoing[gameId]) {
        return;
      }

      const { game } = gamesOnGoing[gameId];

      if (
        game.players[0].id !== socket.id &&
        game.players[1].id !== socket.id
      ) {
        return;
      }

      if (game.players[0].id === socket.id && !game.players[0].ready) {
        game.players[0].ready = true;
        game.playersReady++;
      } else if (game.players[1].id === socket.id && !game.players[1].ready) {
        game.players[1].ready = true;
        game.playersReady++;
      }

      if (game.playersReady === 2) {
        game.startGame();
        io.to(gameId).emit("update", { game: game });
        setTimeout(() => {
          io.to(gameId).emit("endCountdown");
        }, 4500);
      } else {
        if (game.players[0].id === socket.id) {
          io.to(gameId).emit("ready", { player1: true });
        } else {
          io.to(gameId).emit("ready", { player2: true });
        }
      }
    }

    function handleShield(data) {
      console.log("***** MOVE ***** " + socket.id);
      console.log("shield");
      console.log(data);

      const { cell, gameId } = data;

      if (!gamesOnGoing[gameId]) {
        return;
      }

      const { game } = gamesOnGoing[gameId];

      if (
        game.players[0].id !== socket.id &&
        game.players[1].id !== socket.id
      ) {
        return;
      }

      const typeFrom = cell.type;
      while (game) {
        if (!game.moveOnGoing) break;
      }
      if (!game || game.endGame) return;
      let idPlayer = game.grid.grid.find(
        (cell2) =>
          cell2.coords.i === cell.coords.i && cell2.coords.j === cell.coords.j
      ).type;
      if (idPlayer !== typeFrom) {
        socket.emit("cancelMove");
        return (game.moveOnGoing = false);
      }
      game.moveOnGoing = true;
      game.giveShield(cell);
      game.setLastPlayerPlayed(cell.idPlayer);
      io.to(gameId).emit("update", { game });
      if (game.type === "dupz") {
        regainEnergy(gameId, idPlayer);
      }
      game.moveOnGoing = false;
    }

    function handleMove(socket, data, type) {
      //set variables
      console.log("***** MOVE ***** " + socket.id);
      console.log(type);
      console.log(data);
      console.log(socket.id);
      let { cellFrom, cellTo, gameId } = data;

      if (!gamesOnGoing[gameId]) {
        return;
      }

      const { game } = gamesOnGoing[gameId];

      if (
        game.players[0].id !== socket.id &&
        game.players[1].id !== socket.id
      ) {
        return;
      }

      //handle case when 2 players play at the same time in the same area
      //don't do anything if there is a move on going
      //once there isn't a move on going anymore, if the type of the piece of the cell
      //has changed, means the piece got eaten, then returns.
      const typeFrom = cellFrom.type;
      while (game) {
        if (!game.moveOnGoing) break;
      }
      if (!game || game.endGame) return;
      game.moveOnGoing = true;
      let idPlayer = game.grid.grid.find(
        (cell) =>
          cell.coords.i === cellFrom.coords.i &&
          cell.coords.j === cellFrom.coords.j
      ).type;
      let newTypeTo = game.grid.grid.find(
        (cell) =>
          cell.coords.i === cellTo.coords.i && cell.coords.j === cellTo.coords.j
      ).type;
      if (idPlayer !== typeFrom || newTypeTo !== Type.EMPTY) {
        socket.emit("cancelMove");
        return (game.moveOnGoing = false);
      }

      //do the action move
      if (type === "dup") {
        game.duplicatePiece(cellFrom, cellTo);
      } else {
        game.jumpPiece(cellFrom, cellTo);
      }
      if (game.type === "dupz") {
        regainEnergy(gameId, idPlayer);
      }

      //usefull for deselect the cell of the player who played
      game.setLastPlayerPlayed(cellFrom.idPlayer);
      // look if the game is finished
      if (game.endDetection()) {
        io.to(gameId).emit("update", { game: game });
        game.moveOnGoing = false;
        return updateNumbers();
      }

      const lastCellPlayed = game.grid.grid.find(
        (cell) =>
          cell.coords.i === cellTo.coords.i && cell.coords.j === cellTo.coords.j
      );
      //update the game for both players
      io.to(gameId).emit("update", {
        game: game,
        lastCellPlayed,
      });
      game.moveOnGoing = false;
    }

    function handleLeaveRoom(data) {
      console.log("***** LEAVE ROOM ***** " + socket.id);

      if (gamesWaiting.dupz.find((game) => game.players[0].id === socket.id)) {
        gamesWaiting.dupz.shift();
        updateNumbers();
        return;
      }

      if (gamesWaiting.fun.find((game) => game.players[0].id === socket.id)) {
        gamesWaiting.fun.shift();
        updateNumbers();
        return;
      }

      if (
        gamesWaiting.bacteria.find((game) => game.players[0].id === socket.id)
      ) {
        gamesWaiting.bacteria.shift();
        updateNumbers();
        return;
      }

      if (!data) {
        return;
      }
      const { gameId } = data;

      if (!gamesOnGoing[gameId]) {
        return;
      }
      const { game } = gamesOnGoing[gameId];

      if (
        game.players[0].id !== socket.id &&
        game.players[1].id !== socket.id
      ) {
        return;
      }
      socket.leave(gameId);
      let otherPlayerId;
      let notLeaverType;
      if (game && game.players[0].id === socket.id) {
        otherPlayerId = game.players[1].id;
        notLeaverType = 1;
      } else {
        otherPlayerId = game.players[0].id;
        notLeaverType = 0;
      }
      if (io.sockets.sockets[otherPlayerId]) {
        io.sockets.sockets[otherPlayerId].emit("otherLeftRoom");
      }
      if (!game.endGame) {
        game.setEndGame(notLeaverType, false);
      }
      io.to(gameId).emit("update", { game });
      delete gamesOnGoing[gameId];
      updateNumbers();
    }

    function handleDeconnection() {
      console.log(`${socket.id} disconnected`);
      numberUsers--;
      for (let [key, value] of Object.entries(gamesOnGoing)) {
        if (
          socket.id === value.game.players[0].id ||
          socket.id === value.game.players[1].id
        ) {
          var type;
          var dcType;
          if (socket.id === value.game.players[0].id) {
            type = 1;
            dcType = 0;
          } else {
            type = 0;
            dcType = 1;
          }
          value.game.setDisconnected(dcType);

          const playerDisconnected = value.game.players[dcType];

          setTimeout(() => {
            if (
              value.game.players[0].id === "disconnected" ||
              value.game.players[1].id === "disconnected"
            ) {
              console.log("*** END GAME DISCONNECTION *** " + socket.id);
              value.game.setEndGame(type, true);
              value.game.calculateNewRatings();
              io.to(key).emit("game_unpaused", { end: true });
              io.to(key).emit("update", { game: value.game });
              io.to(key).emit("otherLeftRoom");
              delete gamesOnGoing[key];
              updateNumbers(socket);
            }
          }, 30000);
          console.log("WAITING 30SEC FOR RECONNECTION");
          io.to(key).emit("game_paused", { pseudo: playerDisconnected.name });
          return;
        }
      }
      if (gamesWaiting.dupz.length > 0) {
        if (gamesWaiting.dupz[0].players[0].id === socket.id) {
          gamesWaiting.dupz.shift();
        }
      }

      if (gamesWaiting.bacteria.length > 0) {
        if (gamesWaiting.bacteria[0].players[0].id === socket.id) {
          gamesWaiting.bacteria.shift();
        }
      }

      if (gamesWaiting.fun.length > 0) {
        if (gamesWaiting.fun[0].players[0].id === socket.id) {
          gamesWaiting.fun.shift();
        }
      }
      updateNumbers(socket);
    }

    function handleReconnection(data) {
      console.log("***** RECONNECTION ***** " + socket.id);
      console.log(data);

      if (!gamesOnGoing[data.id]) {
        return;
      }

      const { game } = gamesOnGoing[data.id];

      socket.join(data.id);
      if (game.players[0].name === data.pseudo) {
        game.setReconnected(0, socket.id);
      } else if (game.players[1].name === data.pseudo) {
        game.setReconnected(1, socket.id);
      }

      io.to(data.id).emit("update", { game: game });
      io.to(data.id).emit("game_unpaused", { end: false });
      setTimeout(() => {
        io.to(data.id).emit("endCountdown");
      }, 4500);
    }

    function regainEnergy(gameId, idPlayer, twice) {
      let timer = d3Timer.timer((elapsed) => {
        if (gamesOnGoing[gameId]) {
          if (!gamesOnGoing[gameId].game.players[idPlayer].reloading) {
            timer.stop();
            if (twice) regainEnergyForReal(gameId, idPlayer, twice);
            else regainEnergyForReal(gameId, idPlayer, twice);
          }
        } else timer.stop();
      });
    }
    function regainEnergyForReal(gameId, idPlayer, twice) {
      if (gamesOnGoing[gameId]) {
        gamesOnGoing[gameId].game.players[idPlayer].reloading = true;
        let timer = d3Timer.timer((elapsed) => {
          if (!gamesOnGoing[gameId]) {
            timer.stop();
          }

          if (elapsed > 3000) {
            timer.stop();
            gamesOnGoing[gameId].game.players[idPlayer].incrementEnergy();
            gamesOnGoing[gameId].game.players[idPlayer].reloading = false;
            io.to(gameId).emit("updateEnergy", { id: idPlayer });
            if (twice) regainEnergy(gameId, idPlayer);
          }
        }, 150);
      }
    }

    function updateNumbers() {
      const numberGamesWaitingDupz = gamesWaiting["dupz"].length;
      const numberGamesWaitingFun = gamesWaiting["fun"].length;
      const numberGamesWaitingBacteria = gamesWaiting["bacteria"].length;

      let numberGamesOnGoingDupz = 0;

      let numberGamesOnGoingFun = 0;
      let numberGamesOnGoingBacteria = 0;

      const dupzGames = [];
      const funGames = [];
      const bacteriaGames = [];

      for (let [key, value] of Object.entries(gamesOnGoing)) {
        if (value.game.type === "dupz") {
          numberGamesOnGoingDupz++;
          dupzGames.push({
            player1: value.game.players[0].name,
            player2: value.game.players[1].name,
          });
        } else if (value.game.type === "fun") {
          numberGamesOnGoingFun++;
          funGames.push({
            player1: value.game.players[0].name,
            player2: value.game.players[1].name,
          });
        } else if (value.game.type === "bacteria") {
          numberGamesOnGoingBacteria++;
          bacteriaGames.push({
            player1: value.game.players[0].name,
            player2: value.game.players[1].name,
          });
        }
      }

      const usernames = Object.entries(io.sockets.sockets).map((socket) => {
        if (socket[1].user) {
          return socket[1].user.name;
        } else {
          return;
        }
      });

      console.log(dupzGames);

      io.emit("onGoing", {
        numberUsers: Object.keys(io.sockets.sockets).length,
        numberGames: Object.keys(gamesOnGoing).length,
        numberGamesWaitingDupz,
        numberGamesWaitingBacteria,
        numberGamesWaitingFun,
        numberGamesOnGoingFun,
        numberGamesOnGoingDupz,
        numberGamesOnGoingBacteria,
        usernames,
        dupzGames,
        bacteriaGames,
        funGames,
      });
    }
  });
};
